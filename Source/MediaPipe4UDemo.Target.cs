// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;
using System.Collections.Generic;

public class MediaPipe4UDemoTarget : TargetRules
{
	public MediaPipe4UDemoTarget( TargetInfo Target) : base(Target)
	{
		Type = TargetType.Game;
		IncludeOrderVersion = EngineIncludeOrderVersion.Latest;
		DefaultBuildSettings = BuildSettingsVersion.Latest;
		DefaultWarningLevel = WarningLevel.Warning;
		ExtraModuleNames.AddRange( new string[] { "MediaPipe4UDemo" } );
	}
}
