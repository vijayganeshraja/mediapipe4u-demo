[简体中文](./README_CN.md)
<br>
<br>
<p align="center">
<h1  align="center">MediaPipe4U Demo</h1>
<p>
<p align="center">
Demo project for MediaPipe4U
<br>
<br>
<a href=""><img src="https://img.shields.io/badge/Unreal Engine-5.4.1-purple.svg"></a>
<a href=""><img src="https://img.shields.io/badge/Test Pass-M4U 20240522 or later-green.svg"></a>
<p align="center">
<a href="https://github.com/endink/Mediapipe4u-plugin">Get MediaPipe4U Unreal Engine Plugin</a>
</p>
</p>

## Get project source code:
> The texture files of this project are stored using git lfs, so you must use the git client with git lfs to clone this project.
1. Dowload and install git client: https://git-scm.com/downloads.
2. Dowload and install git lfs: https://git-lfs.com/
3. Exucte command in terminal to get source code.

```shell
git lfs clone https://gitlab.com/endink/mediapipe4u-demo.git
```



## How to run this project:

1. Copy Mediapipe4U plugins to this Plugins folder.
2. compile the project use Rider or Visual studio
3. Open UnrealEngine Editor and run the demo


You can use maklink.bat to link the plugin folder with your project. 

1. Open maklink.bat in `Plugins` folder and modify first line:

from
```bat
set DIR="D:\3D_Works\UE\MediaPipe4U\Build" 
``` 
to
```bat
set DIR="<MediaPipe4U Folder>"
```

`MediaPipe4U Folder` is the folder you unzipped into after downloading the MediaPipe4U zip file.

2. save maklink.bat   

3. Run maklink.bat as administrator （Administrator privileges are required to run mklink command）

---   


## FAQ

### Where can I download plugins?

Please visit follow page to find the download plugin link:    
https://github.com/endink/Mediapipe4u-plugin


### Where can I find the document?

Please visit follow page to find the document:       
https://github.com/endink/Mediapipe4u-plugin


### How to fix "The following modules are missing or built with a different engine version" when open project?   
1. Delete follow folder and files in project root folder:   
- DerivedDataCache
- Intermediate
- Saved 
- MediaPipe4U_Demo.sln 

2. Mouse right click "MediaPipe4U_Demo.uproject" and click "Generate Visual Studio project files".

3. Open "MediaPipe4U_Demo.sln " with Visual Studio or Rider and compile project.

### When I update the version of the MediaPipe4U plugin, how do I need to do?

1. Just double click "clean_project.bat", the folder will be deleted.
2. Mouse right click "MediaPipe4U_Demo.uproject" and click "Generate Visual Studio project files".
3. Open "MediaPipe4U_Demo.sln " with Visual Studio or Rider and compile project.
   
> If you accidentally perform a "clean solution" operation that will damage the plugin, please re-download the plugin and copy folders to the plugins directory.

### How to fix "GStreamer-WARNING **: Failed to load plugin 'XXXX\gstassrender.dll'"

This is a known GSteamer dependency warning, and although it is shown in red color, you can ignore it without causing any errors.
